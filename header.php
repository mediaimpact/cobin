<header class="container-fluid text-center">
	<div class="container">
		<div class="row cuerpo">
			<div class="col-lg-4 col-sm-4 col-xs-5">
				<!-- <a id="logo" href="./"><img src="image/logo-original.svg" alt=""></a> -->
				<a id="logo" href="./"><img src="image/logo-original.png" alt=""></a>
<!-- 				<i id="menu-co" class="visible-xs fa fa-bars fa-lg" aria-hidden="true"></i> -->
			</div>
			<div class="col-xs-offset-5 col-xs-2 visible-xs hidden-sm hidden-md align-self-end">
				<i id="menu-co" class="visible-xs fa fa-bars fa-lg" aria-hidden="true"></i>
			</div>
			<div class="col-lg-8 col-sm-8 col-xs-12 text-left align-self-end">
				<nav>
					<ul>
						<li class="btn-ancla" id="md1"><a href="#home">Inicio</a></li>
						<li class="btn-ancla" id="md2"><a href="#cobranza">¿Qué es?</a></li>
						<li class="btn-ancla" id="md3"><a href="#porque">Beneficios</a></li>
						<li class="btn-ancla" id="md4"><a href="#escribenos">Contacto</a></li>
						<li class="btn-ancla pago" id="md5"><a href="#funciona">Pago</a></li>
						<li class="hidden-md face">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcobin.pe%2F&width=145&layout=button&action=like&size=small&show_faces=true&share=true&height=65&appId" width="145" height="22" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	<!-- <i id="menu-co" class="visible-xs fa fa-bars fa-lg" aria-hidden="true"></i> -->
</header>