<footer class="container-fluid text-center">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-2 col-sm-offset-2 col-lg-10 col-sm-10">
				<div class="row links">
					<div class="col-md-4 col-sm-4"><a href="javascript:void(0)" target="_blank">Condiciones de Servicio</a></div>
					<div class="col-md-4 col-sm-4"><a href="javascript:void(0)" target="_blank">Política de Privacidad</a></div>
					<div class="col-md-4 col-sm-4"><a href="javascript:void(0)" target="_blank">Política de Incumplimiento</a></div>
				</div>
			</div>
			<div class="col-lg-12 text-center">
				<div class="col-lg-2 col-sm-2">
					<a href="./"><img src="image/logo-black_1.png" alt="" class="log"></a>
				</div>
				<div class="col-md-10 col-sm-10"><p class="linka">Diseño y desarollo <a href="https://www.mediaimpact.pe/" target="_blank">
					<img src="image/logo-mi.png" alt=""></a></p></div>
				<div class="col-md-12 col-sm-12">
					<p>© Copyright 2018. Todos los derechos reservados. Powered by <a href="javascript:void(0)">Apps2go</a></p>
				</div>				
			</div>
		</div>
	</div>
</footer>